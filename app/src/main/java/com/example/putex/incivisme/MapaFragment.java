package com.example.putex.incivisme;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapaFragment extends Fragment {

    public MapaFragment() {
        // Required empty public constructor
    }
    public static Incidencia incidencia;
    GoogleMap nmap;
    LatLng aux;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mapa, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.g_map);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference incidencies = uid.child("incidencies");

        SharedViewModel model = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        mapFragment.getMapAsync(map -> {
            // Codi a executar quan el mapa s'acabi de carregar.

            map.setMyLocationEnabled(true);
            MutableLiveData<LatLng> currentLatLng = model.getCurrentLatLng();
            LifecycleOwner owner = getViewLifecycleOwner();
            currentLatLng.observe(owner, latLng -> {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                map.animateCamera(cameraUpdate);
                currentLatLng.removeObservers(owner);
            });

            //----------------------- JSON CUSTOMIZATION------------------------
//            try {
//                // Customize the styling of the base map using a JSON object defined
//                // in a raw resource file.
//                boolean success = map.setMapStyle(
//                        MapStyleOptions.loadRawResourceStyle(
//                                getActivity(), R.raw.map_style));
//
//                if (!success) {
//                    Log.e(null, "Style parsing failed.");
//                }
//            } catch (Resources.NotFoundException e) {
//                Log.e(null, "Can't find style. Error: ", e);
//            }


            incidencies.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                     incidencia = dataSnapshot.getValue(Incidencia.class);

                     aux = new LatLng(
                            Double.valueOf(incidencia.getLatitud()),
                            Double.valueOf(incidencia.getLongitud())
                    );



                    Marker marker = map.addMarker(new MarkerOptions()

                            .title(incidencia.getProblema())
                            .snippet(incidencia.getDireccio())
                            .position(aux));
                    nmap=map;


                }



                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });


        });



        return v;
    }


    public class IncidenciesInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
            private final Activity activity;

        public IncidenciesInfoWindowAdapter(Activity activity) {
            this.activity = activity;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }
        @Override
        public View getInfoContents(Marker marker) {

            View view = activity.getLayoutInflater()
                    .inflate(R.layout.info_view, null);

             incidencia = (Incidencia) marker.getTag();

            ImageView ivProblema = view.findViewById(R.id.iv_problema);
            TextView tvProblema = view.findViewById(R.id.tvProblema);
            TextView tvDescripcio = view.findViewById(R.id.tvDescripcio);

            tvProblema.setText(incidencia.getProblema());
            tvDescripcio.setText(incidencia.getDireccio());

            return view;
        }
      public  IncidenciesInfoWindowAdapter customInfoWindow = new IncidenciesInfoWindowAdapter(
                getActivity()



        );




    }


}
